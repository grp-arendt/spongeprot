#! /usr/bin/bash

QUERY_FILE="$1"
DATABASE_DIR="$2"
OUTPUT_DIR="$3"

#change to directory

cd /Volume/arendt/Fabian/PhD/Computational/spongeprot/analysis/phylogenies/MYLK/mmseqs2_search/HSa_MYLK1_domain


# Loop through each FASTA file in the database directory
for DB_FILE in "$DATABASE_DIR"/*.fasta; do
    # Get the filename without the path or extension
    DB_NAME="$(basename "$DB_FILE" .fasta)"
    
    # Run mmseqs2 easy-search with the query file and database file
    mmseqs easy-search "$QUERY_FILE" "$DB_FILE" "${OUTPUT_DIR}/${DB_NAME}_results.m8" tmp

done