#!/bin/bash

#SBATCH -J MYLK_domain
#SBATCH -A arendt
#SBATCH -N 1
#SBATCH -n 20
#SBATCH --mem=10G
#SBATCH -t 40:00:00
#SBATCH -o /g/arendt/Fabian/PhD/Computational/Cluster/increment_%j.out
#SBATCH -e /g/arendt/Fabian/PhD/Computational/Cluster/increment_%j.err
#SBATCH --mail-type=START,END,FAIL
#SBATCH --mail-user=ruperti@embl.de

# Change to working directory

cd /g/arendt/Fabian/PhD/Computational/spongeprot/data/phylogeny/MYLK/iqtree/mmseqs2_search_HSa_MYLK1_bitscore130

# Load and execute IQ-Tree

module load IQ-TREE

iqtree2 -s ../../mmseqs2_search/HSa_MYLK1_domain_MSA/All_MSA_manual_cleaning.fasta -m MFP -B 1000 -alrt 1000 -abayes -nt AUTO
