# Image analysis of 2D brightfield timelapse images for pharmacological testing

These files were used for the analysis and plot generation of the 2D brightfield image time lapses that were taken to observe the sponges reaction to treatment with pharmacological compounds. 

The following files are:

- **Inhibitor-Testing_Label_Sequence_Video_IMPROVED.ijm**: Fiji macro for cropping and brightness/contrast adjustment of the raw image sequence

The processed sequences were then transferred to Ilastik and labels (Incurrent, excurrent, rest) were labelled manually and trained. Afterwards the simple segmentation mask was exported.

- **Inhibitor-Testing_Ilastik_Segmentation_Area-Threshold_R_Excel.ijm.ijm**: Fiji macro that takes the simple segmentation mask and calculates areas of the segmented labels.

- **Inhibitor-Testing_Ilastik_Area_Plot.Rmd**: R markdown script that takes the result files containing the area measurements and plots all available replicates (usually triplicates).


