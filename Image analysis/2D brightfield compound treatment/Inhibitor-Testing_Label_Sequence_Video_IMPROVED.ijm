dir_raw = getDirectory("Choose the directory containing the raw TIF files");
dir_seq = dir_raw + "Seq" + File.separator;

// Create the "Seq" directory if it doesn't exist
File.makeDirectory(dir_seq);

list = getFileList(dir_raw);

for (i = 0; i < list.length; i++) {
    path = dir_raw + list[i];
    
    // Open the image
    open(path);
    
    // Adjust brightness and contrast
    run("Enhance Contrast", "saturated=0.35");
    run("Apply LUT", "stack");
    
    // Apply ROI and crop
    // makeRectangle(50, 50, 200, 200);
    roiManager("Select", 0);
	run("Crop");
    
    // Save the modified image
    saveAs("Tiff", dir_seq + list[i]);
    
    // Close the current image
    close();
}

// Open all images in "Seq" and create a video
dir_output = dir_raw + "output.avi";
list_seq = getFileList(dir_seq);

for (i = 0; i < list_seq.length; i++) {
    path_seq = dir_seq + list_seq[i];
    
    // Open the image
    open(path_seq);
    
    // Add label
    setForegroundColor(255, 255, 255);
    // drawString("Frame " + (i + 1), 10, 10);
    run("Label...", "format=0 starting=0 interval=0.5 x=40 y=40 font=40 text=min range=1-1000 use");
	run("Label...", "format=Text starting=0.0 interval=0.5 x=40 y=80 font=40 text=[8-Bromo-cGMP [1 mM]] range=27-54 use");
    
    // Update the display
    updateDisplay();
    
    // Wait for a short duration (adjust as needed)
    wait(100);
    
    // Close the current image
    close();
}

// Save the video as AVI
run("AVI... ", "compression=JPEG frame=12 save=" + dir_output);
