import sys, os, math, re

from ij 				import IJ, ImagePlus
from ij.io 				import FileSaver
from ij.plugin.frame 	import RoiManager
from ij.plugin 			import ImageCalculator
from ij.process 		import ImageProcessor, StackStatistics
from ij.measure 		import ResultsTable

from inra.ijpb.label 		import LabelImages
from inra.ijpb.morphology 	import Strel3D, Morphology, Reconstruction3D
from sc.fiji.localThickness import LocalThicknessWrapper

#@File (label="Select input directory", style="directory") inputdir
#@File (label="Select output directory", style="directory") outputdir
#@String (label="Base name") base_name
#@String (label="Time Point pattern") time_point_pattern

def natural_sort(l): 
    convert = lambda text: int(text) if text.isdigit() else text.lower()
    alphanum_key = lambda key: [convert(c) for c in re.split('([0-9]+)', key)]
    
    return sorted(l, key=alphanum_key)
    
def convertPointsToSeedsSimple(pointlist, imp):
	''' Converts a list of point rois to an image with single voxel seeds '''
	
	# create empty stack from imp
	seeds = imp.duplicate()
	seeds.getProcessor().setThreshold(255,255,0)
	IJ.run(seeds, "Convert to Mask", "stack")
	seeds.getProcessor().setValue(255)
	for roi in pointlist:
		seeds.setZ(roi.getPosition())		
		roi.drawPixels(seeds.getProcessor())
	
	return seeds	

def splitIncurrentExcurrent(thicknessmap, canals_incurrent_seeds, canals_excurrent_seeds):
	''' Split the internal water into incurrent and excurrent canals '''
	
	print 'slowly expanding incurrent and excurrent canal system one step at the time' 
	thresholds = []
	num = StackStatistics(thicknessmap).max
	max_th = num
	min_th = StackStatistics(thicknessmap).min
	print min_th
		
	while int(num) >= min_th:
		num = num/1.1
		num = int(num)
		if num >= min_th:
			thresholds.append(num)
	
	if min_th not in thresholds:
		thresholds.append(min_th)
		
	thresholds = [i for i in reversed(sorted(thresholds))]
	print thresholds

	canals_in = canals_incurrent_seeds
	canals_ex = canals_excurrent_seeds
	
	for t in thresholds:
		print "current thickness threshold set to " + str(t)	
		
		# get the thresholded thicknessmap without the other canal in it
		thicknessmap_in = thicknessmap.duplicate()
		thicknessmap_in.getProcessor().setThreshold(t, max_th + 1, 1)
		IJ.run(thicknessmap_in, "Convert to Mask", "stack")
		thicknessmap_in = ImageCalculator().run(thicknessmap_in, canals_ex, "Subtract create stack")
			
		# run the reconstruction 
		canals_in = ImagePlus("reconstructed",  Reconstruction3D.reconstructByDilation(canals_in.getStack(), thicknessmap_in.getStack()))
		canals_in = ImageCalculator().run(canals_in, canals_incurrent_seeds, "OR create stack") # add the seeds back in 
			
		# get the thresholded thicknessmap without the other canal in it
		thicknessmap_out = thicknessmap.duplicate()
		thicknessmap_out.getProcessor().setThreshold(t, max_th + 1, 1)
		IJ.run(thicknessmap_out, "Convert to Mask", "stack")
		thicknessmap_out = ImageCalculator().run(thicknessmap_out, canals_in, "Subtract create stack")
		
		# run the reconstruction 
		canals_ex = ImagePlus("reconstructed",  Reconstruction3D.reconstructByDilation(canals_ex.getStack(), thicknessmap_out.getStack()))
		canals_ex = ImageCalculator().run(canals_ex, canals_excurrent_seeds, "OR create stack") # add the seeds back in
					
	# rename for convenience if you want to show the different labels
	canals_in.setTitle("incurrent canals")
	canals_ex.setTitle("excurrent canals")
	
	return canals_in, canals_ex

def convertMasksToLabelImage(masklist, labellist):
	''' Converts the list of masks to a single label image with the label numbers given by labellist '''
	
	print "Generating final label image" 
	labelimp = IJ.createImage("Labeled sponge", "8-bit black", masklist[0].getWidth(), masklist[0].getHeight(), masklist[0].getNSlices())
	
	for i, m in enumerate(masklist): 
		current_label = m.duplicate()
		IJ.run(current_label, "Multiply...", "value=255 stack")
		IJ.run(current_label, "Divide...", "value=255 stack")
		IJ.run(current_label, "Multiply...", "value="+str(labellist[i])+" stack")
	
		labelimp = ImageCalculator().run(labelimp, current_label, "Add create stack")
	
	IJ.run(labelimp, "Glasbey", "")
	
	return labelimp
	

#### ----- processing ----- ####
	
inner_water_files = natural_sort([f for f in os.listdir(str(inputdir)) if 'S4-2_intermediate.tif' in f])

previous_imp = None
for f in inner_water_files:
	
	print f
	S1 = IJ.openImage(os.path.join(str(inputdir), f))
	inner_water = LabelImages.keepLabels(S1, [1])
	
	tissue = LabelImages.keepLabels(S1, [2])	
	IJ.run(tissue, "Multiply...", "value=255 stack")	
	
	time_point = re.findall(time_point_pattern, f)[0]
	Filename = base_name + "_" + time_point
	
	IJ.run(inner_water, "Multiply...", "value=255 stack")	
	
	# change to calculating the thicknessmap 
	if os.path.exists(os.path.join(str(inputdir), (Filename + "_thicknessmap.tif"))):
		print "Using existing thickness map" 
		thickness_map = IJ.openImage(os.path.join(str(inputdir), (Filename + "_thicknessmap.tif")))
	else:
		print "Generating new thickness map" 
		thickness_map = LocalThicknessWrapper().processImage(inner_water)
		FileSaver(thickness_map).saveAsTiff(os.path.join(str(inputdir), (Filename + "_thicknessmap.tif")))
	
	# load seeds from user-defined rois. If there are no rois, just keep going with the previous ones and we see how far we get.
	rm = RoiManager().getInstance()
	rm.reset()
	if os.path.exists(os.path.join(str(inputdir), (Filename + '_gemmule_rois.zip'))):
		rm.reset()
		rm.runCommand("Open", os.path.join(str(inputdir), (Filename + "_gemmule_rois.zip")))
		gemmule_seed_rois = [rm.getRoi(i) for i in range(0, rm.getCount())]
	if os.path.exists(os.path.join(str(inputdir), (Filename + '_incurrent_rois.zip'))):	
		rm.reset()
		rm.runCommand("Open", os.path.join(str(inputdir), (Filename + "_incurrent_rois.zip")))
		incurrent_seed_rois = [rm.getRoi(i) for i in range(0, rm.getCount())]
	if os.path.exists(os.path.join(str(inputdir), (Filename + '_excurrent_rois.zip'))):
		rm.reset()
		rm.runCommand("Open", os.path.join(str(inputdir), (Filename + "_excurrent_rois.zip")))
		excurrent_seed_rois = [rm.getRoi(i) for i in range(0, rm.getCount())]
		
	# make masks from the starting seeds for incurrent and excurrent canals
	gemmule_seed = convertPointsToSeedsSimple(gemmule_seed_rois, thickness_map)
	canals_incurrent_seeds = convertPointsToSeedsSimple(incurrent_seed_rois, thickness_map)
	canals_excurrent_seeds = convertPointsToSeedsSimple(excurrent_seed_rois, thickness_map)
	
	gemmule_seed.setTitle('gemmule')
	canals_incurrent_seeds.setTitle('incurrent')
	canals_excurrent_seeds.setTitle('excurrent')
	
	### proceed to splitting inner water into gemmule, incurrent and excurrent canals

	# define the gemmule
	thickness_map_gemmule = thickness_map.duplicate()
	thickness_map_gemmule.getProcessor().setThreshold(30, 65535, 0)
	IJ.run(thickness_map_gemmule, "Convert to Mask", "stack")

	gemmule_seed = ImageCalculator().run(gemmule_seed, inner_water, "AND create stack")
	gemmule = ImagePlus("gemmule reconstructed",  Reconstruction3D.reconstructByDilation(gemmule_seed.getStack(), thickness_map_gemmule.getStack()))
	
	# dilate gemmule using native method or morpholibj
	#native method
	IJ.run(gemmule, "Dilate (3D)", "stack")
	IJ.run(gemmule, "Dilate (3D)", "stack")
	gemmule = ImageCalculator().run(gemmule, tissue, "subtract create stack")
	gemmule.hide()
	
	# morpholibj alternative but slower
#	strel = Strel3D.Shape.BALL.fromRadius(2)
#	gemmule = ImagePlus("gemmule", Morphology.dilation(gemmule.getStack(), strel))
			
	# subtract the gemmule from the canals
	canals = ImageCalculator().run(inner_water, gemmule, "subtract create stack")
	
	# make sure that the seeds only contain available areas (that are not gemmule and that are part of the canals)
	canals_incurrent_seeds = ImageCalculator().run(canals_incurrent_seeds, canals, "AND create stack")
	canals_incurrent_seeds = ImageCalculator().run(canals_incurrent_seeds, gemmule, "Subtract create stack")
	canals_excurrent_seeds = ImageCalculator().run(canals_excurrent_seeds, canals, "AND create stack")
	canals_excurrent_seeds = ImageCalculator().run(canals_excurrent_seeds, gemmule, "Subtract create stack")
	
	# subtract gemmule from thickness_map and set very low values to NaN to prevent that the threshold will be set to zero in the for loop.
	thickness_map_canals = ImageCalculator().run(thickness_map, canals, "AND create stack")	
	thickness_map_canals.getProcessor().setThreshold(0.0000001, 65535, 1)
	IJ.run(thickness_map_canals, "NaN Background", "stack")
	canals_in, canals_ex = splitIncurrentExcurrent(thickness_map_canals, canals_incurrent_seeds, canals_excurrent_seeds)
	
	choanocyte_chambers = ImageCalculator().run(canals, canals_in, "Subtract create stack")
	choanocyte_chambers = ImageCalculator().run(choanocyte_chambers, canals_ex, "Subtract create stack") 
		
	# check if the previous segmentation can change choanocyte chambers in to canals_in or canals_ex
	if previous_imp is not None: 
		prev_canals = LabelImages.keepLabels(previous_imp, [1,2])
		canals_reconst = ImagePlus("reconstructed",  Reconstruction3D.reconstructByDilation(prev_canals.getStack(), choanocyte_chambers.getStack()))
		canals_in_extra = LabelImages.keepLabels(canals_reconst, [1])
		canals_ex_extra = LabelImages.keepLabels(canals_reconst, [2])
		IJ.run(canals_in_extra, "Multiply...", "value=255 stack")
		IJ.run(canals_ex_extra, "Multiply...", "value=255 stack")
		canals_in = ImageCalculator().run(canals_in_extra, canals_in, "Add create stack")
		canals_ex = ImageCalculator().run(canals_ex_extra, canals_ex, "Add create stack")
		
		choanocyte_chambers = ImageCalculator().run(canals, canals_in, "Subtract create stack")
		choanocyte_chambers = ImageCalculator().run(choanocyte_chambers, canals_ex, "Subtract create stack") 
		
	labelled_sponge = convertMasksToLabelImage([canals_in, canals_ex, gemmule, choanocyte_chambers, tissue], [1,2,3,4,5])		
	FileSaver(labelled_sponge).saveAsTiff(os.path.join(str(outputdir), (Filename +"_S3_intermediate.tif")))
	
	gemmule.close()
	previous_imp = labelled_sponge	
