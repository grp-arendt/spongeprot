''' FIJI jython script to separate internal from external water in a sponge foreground/background segmentation '''

import os, sys, re
from ij 				import IJ, ImagePlus
from ij.io 				import FileSaver
from ij.plugin 			import ImageCalculator, Slicer, Resizer
from ij.process 		import StackProcessor

from inra.ijpb.binary import BinaryImages
from inra.ijpb.morphology import Strel3D, Morphology

def natural_sort(l): 
    convert = lambda text: int(text) if text.isdigit() else text.lower()
    alphanum_key = lambda key: [convert(c) for c in re.split('([0-9]+)', key)]
    
    return sorted(l, key=alphanum_key)

def scaleWithoutAveraging(imp, newWidth, newHeight, newDepth):
	''' function to resize an image without averaging when downsizing to maintain it as a binary image '''
	
	Resizer().setAverageWhenDownsizing(False)
	imp_resized = imp.duplicate()
	imp_resized = ImagePlus("imp small", StackProcessor(imp_resized.getStack()).resize(newWidth, newHeight, 0))
	imp_resized = Resizer().zScale(imp_resized, newDepth, 0)
	
	return imp_resized

def convertMasksToLabelImage(masklist, labellist):
	''' Converts the list of mvolumeOpening​(ij.ImageStack image, int nVoxelMin)asks to a single label image with the label numbers given by labellist '''
	
	print "Generating final label image" 
	labelimp = IJ.createImage("Labeled sponge", "8-bit black", masklist[0].getWidth(), masklist[0].getHeight(), masklist[0].getNSlices())
	
	for i, m in enumerate(masklist): 
		current_label = m.duplicate()
		IJ.run(current_label, "Divide...", "value=255 stack")
		IJ.run(current_label, "Multiply...", "value="+str(labellist[i])+" stack")
	
		labelimp = ImageCalculator().run(labelimp, current_label, "Add create stack")
	
	IJ.run(labelimp, "Glasbey", "")
	
	return labelimp
	
# close open images and open the sponge image
IJ.run("Close All")
IJ.run("Collect Garbage", "")

#@File (label="Select input directory", style="directory") inputdir
#@File (label="Select output directory", style="directory") outputdir

# Find all images in the inputdir
files = [f for f in os.listdir(str(inputdir)) if '.tif' in f]
files = natural_sort(files)

for f in files:
	
	IJ.run("Collect Garbage", "")
	
	# Open image
	print 'opening image ' + f + ' and proceeding to artificially closing the sponge to separate outside environment from inside environment'
	imp = IJ.openImage(os.path.join(str(inputdir), f))
	Filename = imp.getShortTitle()
	IJ.run(imp, "Options...", "iterations=1 count=1 black do=Nothing") # uncheck 'black background' 
	
	# keep the largest connected region
	imp = ImagePlus('largest regions', BinaryImages.volumeOpening(imp.getStack(), 100000))
#	imp = BinaryImages.keepLargestRegion(imp) 
	
	### Divide inside and outside of the sponge ###
	# downsize the image to make it easier to work with
	imp_small = scaleWithoutAveraging(imp, int(imp.getWidth() * .25), int(imp.getHeight() * .25), int(imp.getNSlices() * .25))
	
	# dilate the image and fill holes in all directions
	strel = Strel3D.Shape.BALL.fromRadius(4)
	dil = ImagePlus("dilated", Morphology.dilation(imp_small.getStack(), strel))
	IJ.run(dil, "Fill Holes", "stack")
	dil = Slicer().reslice(dil)
	IJ.run(dil, "Fill Holes", "stack")
	IJ.run(dil, "Rotate 90 Degrees Left", "")
	dil = Slicer().reslice(dil)
	IJ.run(dil, "Fill Holes", "stack")
	IJ.run(dil, "Rotate 90 Degrees Left", "")
	dil = Slicer().reslice(dil)
	IJ.run(dil, "Fill Holes", "stack")
	IJ.run(dil, "Rotate 90 Degrees Right", "")
	IJ.run(dil, "Flip Z", "")
	
	# erode the image to get back to original size. Discard small fragments
	ero = ImagePlus("filled sponge", Morphology.erosion(dil.getStack(), strel))
	ero_filtered = BinaryImages.keepLargestRegion(ero)
	
	# resize back to original dimensions
	filled_sponge = scaleWithoutAveraging(ero_filtered, int(imp.getWidth()), int(imp.getHeight()), int(imp.getNSlices()))

	# do some erosion to reduce taking up water from outside at the borders
	IJ.run(filled_sponge, "Erode (3D)", "stack")
	IJ.run(filled_sponge, "Erode (3D)", "stack")
	IJ.run(filled_sponge, "Erode (3D)", "stack")
	filled_sponge.hide()	
		
	### define inside and outside water ###
	inner_water = ImageCalculator().run(filled_sponge, imp, "Subtract create stack")
	
	### combine inner_water and tissue and fill small gaps
	combined = ImageCalculator().run(inner_water, imp, "Add create stack")
	IJ.run(combined, "Fill Holes", "stack")	
	combined = Slicer().reslice(combined)
	IJ.run(combined, "Fill Holes", "stack")
	IJ.run(combined, "Rotate 90 Degrees Left", "")
	combined = Slicer().reslice(combined)
	IJ.run(combined, "Fill Holes", "stack")
	IJ.run(combined, "Rotate 90 Degrees Left", "")
	combined = Slicer().reslice(combined)
	IJ.run(combined, "Fill Holes", "stack")
	IJ.run(combined, "Rotate 90 Degrees Right", "")
	IJ.run(combined, "Flip Z", "")

	inner_water = ImageCalculator().run(combined, imp, "Subtract create stack")
	
	### combine into label mask and save
	labels = convertMasksToLabelImage([inner_water, imp], [1,2])

	FileSaver(labels).saveAsTiff(os.path.join(str(outputdir), (Filename.split('Simple')[0] + 'S4-2_intermediate.tif')))
	
