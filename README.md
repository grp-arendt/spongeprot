# Molecular profiling of sponge deflation reveals an ancient relaxant-inflammatory response 

This repository contains all scripts and notebooks used for data analysis and exploration of the paper
"Molecular profiling of sponge deflation reveals an ancient relaxant-inflammatory response" (Ruperti, et al., 2023)

Sponges (Porifera) are animals that lack neurons or muscle cells. However they are able to undergo whole body deflations, cleaning the sponge canal system. In this work, we are investigating the morphological changes (**What?**), their underlying molecular mechanism (**How?**) and the physiological responses (**Why?**) of agitation stress and Nitric oxide -induced movement in the freshwater sponge *Spongilla lacustris*.

The repository is structured as following:
- Image segmentation and analysis (Optical coherence microscopy and 2D brightfield): **Image analysis**
- Analysis and exploration of proteomics, single cell RNA seq and metabolomics data : **Data analysis and exploration**
- Phylogenetic analysis of MYLK domains: **Phylogenetic analysis**

Relevant and additional files, images and videos are deposited in [Zenodo](https://doi.org/10.5281/zenodo.8116913).

## Authors and contributions

- Fabian Ruperti, Jacob Musser and Detlev Arendt conceived the project.
- Fabian Ruperti, Jacob Musser, Mikhail M. Savitski, Isabelle Becher and Detlev Arendt designed the project.
- Fabian Ruperti and Michael Nickel collected sponge gemmules.
- Isabelle Becher, Clement Potel and Fabian Ruperti performed the proteomic experiments.
- Ling Wang and Fabian Ruperti performed the OCM experiments.
- Fabian Ruperti, Nick Marschlich and Klaske Schippers performed the pharmacological treatments, confocal imaging and image analysis.
- Bernhard Drotleff and Fabian Ruperti performed the untargeted metabolomic experiments and analysis.
- Anniek Stokkermans, Ling Wang and Emanuel Maus performed the OCM data segmentation and analysis.
- Frank Stein, Isabelle Becher, Clement Potel and Mikhail M. Savitski designed and implemented the proteomic data analysis strategies.
- Fabian Ruperti performed the proteomic data analysis.
- Michael Nickel, Klaske Schippers and Robert Prevedel contributed to the manuscript and gave advice.
- Fabian Ruperti, Jacob Musser, and Detlev Arendt wrote the manuscript.

## License
GPL 3.0
